<?php

class LalafoAdmin {
  public $login = '';
  public $password = '';
  public $token = null;
  public $response = null;
  public $baseURI = 'https://api.lalafo.com';
  public $ad = null;

  public function __construct($options) {
    $this->login = $options['login'];
    $this->password = $options['password'];
  }
  
function build_data_files($boundary, $fields, $files){
    $data = '';
    $eol = "\r\n";

    $delimiter = '---------AndroidUploadService' . $boundary;

    foreach ($fields as $name => $content) {
        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
            . $content . $eol;
    }


    foreach ($files as $name => $content) {
        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="image_file"; filename="' . basename($name) . '"' . $eol
            . 'Content-Type: image/jpg'.$eol
            //. 'Content-Transfer-Encoding: binary'.$eol
            ;

        $data .= $eol;
        $data .= $content . $eol;
    }
    $data .= "--" . $delimiter . "--".$eol;


    return $data;
}

  public function request ($method, $uri, $data, $filesArr = null) {
    $ch = curl_init();
    $headers = [
      "app_version: 9999",
      "country-id: 12",
      "device: android",
      "language: ru_RU",
      "user-agent: okhttp/3.11.0",
    ];

    if (!empty($this->token)) {
        $headers[] = 'Authorization: Bearer '. $this->token;
    }
    
    $opts = [
      CURLOPT_URL => "https://api.lalafo.com{$uri}",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_SSLCERT => dirname(__FILE__) . '/cert/lalafo.crt',
      CURLOPT_SSLKEY => dirname(__FILE__) . '/cert/lalafo.key',
      CURLOPT_SSLCERTPASSWD => '1111',
      CURLOPT_SSLKEYPASSWD => '1111',
      CURLOPT_CUSTOMREQUEST => $method,
    ];
    
    if (!empty($filesArr)) {
        $files = array();
        foreach ($filesArr as $f){
           $files[$f] = file_get_contents($f);
        }
        
        $boundary = uniqid();
        $delimiter = '---------AndroidUploadService' . $boundary;
        $post_data = $this->build_data_files($boundary, $data, $files);
        $headers[] = "content-type: multipart/form-data; boundary=" . $delimiter;
        $headers[] = "content-length: " . strlen($post_data);
        $opts[CURLOPT_POST] = true;
        $opts[CURLOPT_POSTFIELDS] = $post_data;
    } else if(!empty($data)) {
        $headers[] = "content-type: application/json";
        $opts[CURLOPT_POST] = true;
        $opts[CURLOPT_POSTFIELDS] = json_encode($data);
    }
    $opts[CURLOPT_HTTPHEADER] = $headers;
    curl_setopt_array($ch, $opts);
    
    $response = curl_exec($ch);
    $err = curl_error($ch);

    curl_close($ch);

    if ($err) {
      return null;
    } else {
      return json_decode($response);
    }
  }

  public function getToken () {
    $laLogin =  strpos($this->login, '@') !== false ? 'email' : 'mobile';
    $data = [
      $laLogin => $this->login,
      'password' => $this->password
    ];
    $res = $this->request('POST', '/v3/users/login', $data);

    if (isset($res->token)) {
      $this->token = $res->token;
      return $res->token;
    }
    
    throw new Exception(json_encode($res));
  }

  public function setToken ($token) {
    $this->token = $token;
  }

  public function createTempAd () {
    $res = $this->request('POST', '/v32/posting-ads/temp', null);
    if (isset($res->id)) {
        $this->ad = $res;
        return $res;
    }
    
    throw new Exception(json_encode($res));
  }

  public function setTempAd ($obj) {
    $this->ad = $obj;
  }
  
  public function uploadImages ($files) {
    $data = [
        'ad_id' => $this->ad->id
    ];
    
    $res = $this->request('POST', '/v3/images/upload', $data, $files);
    return $res;
  }

  public function putData ($data) {
    $res = $this->request('PUT', '/v32/posting-ads/temp/'. $this->ad->id, $data);
    if (isset($res->id)) {
      return $res;
    }
    throw new Exception(json_encode($res));
  }

  public function publish () {
    $res = $this->request('POST', '/v32/posting-ads/temp/'. $this->ad->id . '/publish?expand=available_campaign_types', null);
    if (isset($res->id)) {
      return $res;
    }
    throw new Exception(json_encode($res));
  }
}