<?php
/**
 * Mini api for manage with settings table Lalafo
 * @author Абсатар уулу Мыктыбек
 * @version 1.0
 */

$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';
global $wpdb;

/**
 * Create nessary tables
 */
if($_POST['Method'] == 'Install')
{
	try 
	{
		if($wpdb->get_var("SHOW TABLES LIKE 'wp_lalafo_user'") != 'wp_lalafo_user')
		{
			$create_table_wp_lalafo_user = "
	            CREATE TABLE IF NOT EXISTS `wp_lalafo_user` (
	              `Id` int NOT NULL AUTO_INCREMENT,
	              `Login` varchar(50) NOT NULL,
	              `Password` varchar(50) NOT NULL,
	              `ProductCount` int NOT NULL,
	              `IsActive` Boolean NOT NULL,	              
	              PRIMARY KEY (Id)
	            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	    	";
	    	$create_table_wp_lalafo_settings = "
				CREATE TABLE IF NOT EXISTS `wp_lalafo_settings` (
	              `Id` int NOT NULL AUTO_INCREMENT,
	              `LalafoId` int NOT NULL,
	              `WooComId` int NOT NULL,
	              PRIMARY KEY (Id)
	            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	    	";
	    	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	    	dbDelta( $create_table_wp_lalafo_user );
	    	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	    	dbDelta( $create_table_wp_lalafo_settings );
	    	echo 'true';
		}
		else
		{
			echo 'true';
		}
	} 
	catch (Exception $e) 
	{
    	echo 'Ошибка установки необходимых таблиц плагина: ',  $e->getMessage(), "\n";
	}
}

/**
 * Get user information
 */
if($_POST['Method'] == 'GetUserInfo')
{
	$res = $wpdb->get_results('SELECT * FROM wp_lalafo_user');
	echo json_encode($res);
}

/**
 * Get categories
 */
if($_POST['Method'] == 'GetCategories')
{
	$res = $wpdb->get_results
	(
		"SELECT wp_terms.* 
    	 FROM wp_terms 
    	 LEFT JOIN wp_term_taxonomy ON wp_terms.term_id = wp_term_taxonomy.term_id
    	 WHERE wp_term_taxonomy.taxonomy = 'product_cat'"
    );
	echo json_encode($res);
}

/**
 * Save Lalafo Login and Password
 */
if($_POST['Method'] == 'Register')
{
	$Login = $_POST['la_login'];
	$Password = $_POST['la_password'];
	$ProductCount = $_POST['la_product_count'];
	$res = $wpdb->get_results('SELECT * FROM wp_lalafo_user');
	
	if(empty($res))
	{
		$wpdb->insert('wp_lalafo_user', array('Login'=>$Login, 'Password'=>$Password, 'IsActive' => true, 'ProductCount' => $ProductCount));
	}

	if(!empty($res))
	{
		foreach($res as $r)
		{
			$wpdb->update('wp_lalafo_user', array('Login'=>$Login, 'Password'=>$Password, 'ProductCount' => $ProductCount), array('Id'=>$r->Id));
		}
	}
	echo 'Данные успешно сохранены';
}

/**
 * Insert joined products
 */
if($_POST['Method'] == 'InsertProducts')
{
	$la_products_shop = $_POST['la_products_shop'];
	$la_products_lalafo = $_POST['la_products_lalafo'];
	$res = $wpdb->get_results('SELECT * FROM wp_lalafo_settings WHERE WooComId = $la_products_shop AND LalafoId = $la_products_lalafo');
	if(empty($res))
	{
		$wpdb->insert('wp_lalafo_settings', array('WooComId'=>$la_products_shop, 'LalafoId'=>$la_products_lalafo));
	}
}

/**
 * Get saved products
 */
if($_POST['Method'] == 'GetProducts')
{
	$data = $wpdb->get_results('SELECT * FROM wp_lalafo_settings ORDER BY Id DESC');
	$table = '<table class="table table-bordered">
	          <thead>
	            <tr>
	              <th>#</th>
	              <th>Продукты Магазина</th>
	              <th>Продукты Lalafo</th>
	              <th>Удалить</th>
	            </tr>
	          </thead>
	          <tbody>';
	foreach($data as $row)
	{
		$table .= '<tr>
			        <th scope="row">'.($i+=1).'</th>
			        <td>'.$row->WooComId.'</td>
			        <td>'.$row->LalafoId.'</td>
			        <td scope="row"><button type="button" class="btn btn-danger" onclick="RemoveProduct('.$row->Id.')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>
			       </tr>';
	}
	$table .= '</tbody>
	          </table>';
	echo $table;
}

/**
 * Remove product by Id
 */
if($_POST['Method'] == 'DeleteProduct')
{
	$Id = $_POST['Id'];
	$wpdb->delete('wp_lalafo_settings', array('Id' => $Id));
}

/**
 * Change run status
 */
if($_POST['Method'] == 'ChangeStatus')
{
	$IsActive = $_POST['IsActive'];
	$res = $wpdb->get_results('SELECT * FROM wp_lalafo_user');

	if(!empty($res))
	{
		foreach($res as $r)
		{
			$wpdb->update('wp_lalafo_user', array('IsActive' => $IsActive), array('Id'=>$r->Id));
		}
	}
	echo 'true';
}