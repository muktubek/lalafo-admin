<?php
/*
Plugin Name: Lalafo Администратор
Description: Плагин для публикации продуктов на сайте lalafo.kg
Author: Абсатар уулу Мыктыбек
Version: 0.1
*/
require dirname(__FILE__) .'/LalafoAdmin.class.php';

add_action('admin_menu', 'lalafo_admin');

/**
 * Install
 */
function lalafo_admin(){
	add_menu_page( 'Lalafo', 'Lalafo', 'manage_options', 'lalafo', 'lalafo_init', 'https://lalafo.kg/favicons/favicon-16x16.png');
}

/**
 * Show lalafo settings page
 */
function lalafo_init(){    
    // JS
    wp_register_script('prefix_bootstrap', 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js');
    wp_register_script('prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');
    wp_enqueue_script('prefix_bootstrap');

    // CSS
    wp_register_style('prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
    wp_register_style('prefix_bootstrap', 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css');
    wp_enqueue_style('prefix_bootstrap');
	?>
	<style type="text/css">
		.update-nag, #dolly, #setting-error-tgmpa, .woodmart-msg, .updated{
			display: none;
		}
		.la_form{
			width: 250px;
			margin: 25px;
			height: 35px;
		}
		.la_row{
			width: auto;
			background: white;
			padding: 10px;
			margin: 25px;
		}
    #instruction{
      color: green;
    }
	</style>
    <div class="la_row">
		<div class="panel panel-default">
  			<div class="panel-heading">
    			<h3 class="panel-title">Настойки учетных данных пользователя Lalafo</h3>
  			</div>
  			<div class="panel-body">
    			<div class="row">
  					<div class="col-md-3">
  						<div class="form-group">
    						<label for="la_login">Логин</label>
    						<input type="text" class="form-control" id="la_login" placeholder="Логин">
  						</div>
  					</div>
  					<div class="col-md-3">
  						<div class="form-group">
    						<label for="la_password">Пароль</label>
    						<input type="password" class="form-control" id="la_password" placeholder="Пароль">
  						</div>
  					</div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="la_product_count">Количество продукта</label>
                <select class="form-control" id="la_product_count" title="Количество продукта для загрузки при каждом выполнение задачи"></select>
              </div>
            </div>
  					<div class="col-md-3">
  						<div class="form-group">
    						<label for="la_btnSaveUser"><br></label>
    						<input type="submit" class="form-control btn btn-default" id="la_btnSaveUser" value="Сохранить">
  						</div>
  					</div>
            <div class="col-md-12">
              <div id = "instruction"></div>
            </div>
				</div>
  			</div>
		</div>
		<div class="panel panel-default">
  			<div class="panel-heading">
    			<h3 class="panel-title">Настойки категории продуктов для публикации</h3>
  			</div>
  			<div class="panel-body">
    			<div class="row">
  					<div class="col-md-3">
  						<div class="form-group">
    						<label for="la_products_shop">Продукты магазина</label>
    						<select class="form-control la_select" id="la_products_shop"></select>
  						</div>
  					</div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="la_products_lalafo">Продукты Lalafo</label>
                 <select class="form-control la_select" id="la_products_lalafo"></select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="la_start">Состояние</label>
                <select class="form-control" id="la_start">
                    <option style="display:none"></option>
                    <option value="1">Запущен</option>
                    <option value="0">Остановлен</option>
                </select>
              </div>
            </div>
  					<div class="col-md-3">
  						<div class="form-group">
    						<label for="la_btnSave"><br></label>
    						<input type="submit" class="form-control btn btn-default" id="la_btnSaveProducts" value="Добавить">
  						</div>
  					</div>
				</div>
        <div id="results"></div>
  			</div>
		</div>
	</div>
	<script type="text/javascript">
    var url = window.location.protocol+'//'+window.location.hostname+'/wp-content/plugins/lalafo_admin/app.php';
    jQuery(document).ready(function($){
      $(".la_select").select2();
      $(".la_select").select2({dropdownAutoWidth : true});
      $("#instruction").html("<br><b>Используйте данную ссылку для запуска задачи:</b> "+window.location.protocol+'//'+window.location.hostname+'?lalafo_start')
      // Check install
      $.post(url, { Method: 'Install' }, function(res){
          if(res == 'true')
          {              
              InitProductCount();

              // Get login and password if has user data
              $.post(url, { Method: 'GetUserInfo' }, function(result){
                var data = $.parseJSON(result);
                if(data.length > 0){
                    $('#la_login').val(data[0].Login);
                    $('#la_password').val(data[0].Password);
                    $('#la_product_count').val(data[0].ProductCount);
                    $('#la_start').val(data[0].IsActive);
                }
              });

              // Get categories
              $.post(url, { Method: 'GetCategories' }, function(result){
                  var data = $.parseJSON(result);
                  if(data.length > 0){          
                      var output = [];
                      $.each(data, function(key, value)
                      {
                          output.push('<option value="'+ value.term_id +'">'+ value.term_id+' - '+value.name +'</option>');
                      });
                      $('#la_products_shop').html(output.join(''));
                  }
              });

              // Get categories Lalafo
              $.post(window.location.protocol+'//'+window.location.hostname+'/wp-content/plugins/lalafo_admin/categoriesLalafo.json', function(result){
                  var output = [];
                  for(var i = 0; i <= result.length-1; i++){
                    var child1 = result[i].children;
                    if(child1.length > 0){
                      for(var j = 0; j <= child1.length-1; j++){
                        var child2 = child1[j].children;
                        if(child2.length > 0){
                          for(var x = 0; x <= child2.length-1; x++){
                            var child3 = child2[x].children;
                            if(child3.length > 0){
                              for(var y = 0; y <= child3.length-1; y++){
                                output.push('<option value="'+ child3[y].id +'">'+ child3[y].id+' - '+result[i].name+' > '+child1[j].name+' > '+child2[x].name+' > '+child3[y].name +'</option>');
                              }
                            }else{
                              output.push('<option value="'+ child2[x].id +'">'+ child2[x].id+' - '+result[i].name+' > '+child1[j].name+' > '+child2[x].name +'</option>');
                            }
                          }
                        }else{
                          output.push('<option value="'+ child1[j].id +'">'+ child1[j].id+' - '+result[i].name+' > '+child1[j].name +'</option>');
                        }
                      }
                    }else{
                      output.push('<option value="'+ result[i].id +'">'+ result[i].id+' - '+result[i].name +'</option>');
                    }
                  }
                  $('#la_products_lalafo').html(output.join(''));
              });
 
              // Save Lalafo login and password
              $('#la_btnSaveUser').click(function(){
                  var la_login = $('#la_login').val();
                  var la_password = $('#la_password').val();
                  var la_product_count = $('#la_product_count').val();                  
                  if($.trim(la_login) == '')
                    return alert('Введите логин пользователя пожалуйста');
                  if($.trim(la_password) == '')
                    return alert('Введите пароль пользователя пожалуйста');
                  $.post(url, { Method: 'Register', la_login: la_login, la_password: la_password, la_product_count: la_product_count }, function(result){
                      alert(result);
                  });
              });

              // Save joined product data
              $('#la_btnSaveProducts').click(function(){
                  var la_products_shop = $('#la_products_shop').val();
                  var la_products_lalafo = $('#la_products_lalafo').val();
                  $.post(url, {Method: 'InsertProducts', la_products_shop: la_products_shop, la_products_lalafo: la_products_lalafo}, function(result){
                      $.post(url, {Method: 'GetProducts'}, function(data){
                        $('#results').html(data);
                      });
                  })
              });

              // Get saved products
              $.post(url, {Method: 'GetProducts'}, function(data){
                  $('#results').html(data);
              });

              // Change run status
              $( "#la_start" ).change(function() {
                  var la_start = $('#la_start').val();
                  $.post(url, {Method: 'ChangeStatus', IsActive: la_start}, function(result){
                    $('#la_start').val(la_start);
                  });
              });
          }
          else
          {
            alert(res);
          }
      });
	});
  
  /**
   * Remove selected product
   */
  function RemoveProduct(Id){
      jQuery(document).ready(function($){
          $.post(url, {Method: 'DeleteProduct', Id: Id}, function(result){
              // Get saved products
              $.post(url, {Method: 'GetProducts'}, function(data){
                  $('#results').html(data);
              });
          });
      });
  }

  function InitProductCount(){
    jQuery(document).ready(function($){
      var output = [];
      output.push('<option style="display:none"></option>');
      for(var i = 1; i <= 6; i++){
        var count = i == 1 ? 5 : (10 * (i - 1));
        output.push('<option value="'+ count +'">'+count+'</option>');
      }
      $('#la_product_count').html(output.join(''));
    });
  }
	</script>
<?php
}

function lalafo_get_post_images ($post_id) {
    $urls = [];
    $args = array( 
        'post_type' => 'attachment', 
        'post_mime_type' => 'image',
        'numberposts' => 9, 
        'post_status' => null, 
        'post_parent' => $post_id
    );
    $posts = get_posts( $args );
    foreach($posts as $post) {
        array_push($urls, $post->guid);
    }
    return $urls;
}

function lalafo_get_products ($limit = 10, $product_cat = 0) {
    $args = array(
        "post_type" => "product",
        'posts_per_page' => $limit,
        'meta_query' => 
        [
            [
                'key' => 'lalafo_published',
                'compare' => 'NOT EXISTS'
            ],
            [
                'key' => '_thumbnail_id',
                'compare' => 'EXISTS'
            ]
        ],        
        'tax_query' => 
        [
            [
              'taxonomy' => 'product_cat',
              'terms' => $product_cat,
              'operator' => 'IN'
            ]
        ],
        'orderby' => 'date',
        'order' => 'DECS'
    );
    $query = new WP_Query;
    return $query->query($args);
}

function lalafo_set_published ($post_id) {
    return add_post_meta($post_id, 'lalafo_published', true, true);
}

function lalafo_normalize_price ($str) {
    preg_match('/([0-9]+)\.?/', $str, $matches);
    if (isset($matches[1])) {
        return $matches[1];
    }
    return '';
}

function lalafo_start_init( ) { 
    global $wpdb;
    if (!isset($_GET['lalafo_start'])) {
        return false;
    }
    try {
        $login = '';
        $password = '';
        $IsActive = false;
        $ProductCount = 0;
        $user = $wpdb->get_results('SELECT * FROM wp_lalafo_user');
        foreach($user as $u)
        {
            $login = $u->Login;
            $password = $u->Password;
            $IsActive = $u->IsActive;
            $ProductCount = $u->ProductCount;
        }
        if(!$IsActive)
          return;

        $lalafo = new LalafoAdmin(['login' => $login, 'password' => $password]);
        $lalafo->getToken();
        $categories = $wpdb->get_results('SELECT * FROM wp_lalafo_settings');
        foreach($categories as $category){
          $posts = lalafo_get_products($ProductCount, $category->WooComId);
          foreach($posts as $post) {
              $product = wc_get_product($post);
              $id = $product->get_id();
              $images = lalafo_get_post_images($id);
              echo lalafo_normalize_price($product->get_price()) . "\n";
              
              $lalafo->createTempAd();
              sleep(1);
              $lalafo->uploadImages($images);
              sleep(1);
              $lalafo->putData([
                  'params' => [
                    [
                      'name' => 'Состояние',
                      'id' => 29,
                      'value_id' => 2757,
                      'value' => 'Новый'
                    ]
                  ],
                  'description' => $product->get_title() . "\n" . $product->get_description(),
                  'category_id' => $category->LalafoId,
                  'price' => lalafo_normalize_price($product->get_price()),
                  'currency' => 'KGS',
              ]);
              sleep(1);
              $lalafo->publish();
              lalafo_set_published($id);
              sleep(1);
          }
        }
    } catch (Exception $e) {
      // echo $e->getMessage();
    }
}

add_action( 'init', 'lalafo_start_init', 10, 0 );

?>